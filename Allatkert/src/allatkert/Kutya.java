package allatkert;

import java.util.Random;

public class Kutya extends Allat{
    
    

    //public String nev;
    private Boolean him;
    private Integer dns;//olyan tulajdnoság, ami nem módosítható, és nem is nézhető meg
    private FajtaEnum fajta;
    
    public Kutya(){
        super(0, "Új Kutya");
        this.him = Boolean.FALSE;
        this.dns = 5000;
    }
    
    public Kutya(String ujNev, Integer ujKor, Boolean ujNem, FajtaEnum fajta){
        super(ujKor, ujNev);
        this.him = ujNem;
        this.dns = 1000;
        this.fajta = fajta;
    }
    
    public Boolean isHim() {
        return this.him;
    }

    public FajtaEnum getFajta() {
        return fajta;
    }
    
    //a kutya neme nem módosítható
    
    public void ugat(){
        System.out.println("Vau-vau-vau (" + this.nev + ")");
    }
    
    public Kutya parosodik(Kutya masikKutya){
        Kutya utod = new Kutya();
        utod.kor = 0;
        Random rnd = new Random();
        utod.him = rnd.nextBoolean();
        utod.nev = this.nev + " " + masikKutya.nev;
        utod.dns = this.dns / 2 + masikKutya.dns / 2;
        utod.fajta = masikKutya.fajta;
        return utod;
    }
    
    public static Kutya parosodas(Kutya egyik, Kutya masik){
        Kutya utod = new Kutya();
        utod.kor = 0;
        Random rnd = new Random();
        utod.him = rnd.nextBoolean();
        utod.nev = egyik.nev + " " + masik.nev;
        utod.dns = egyik.dns / 2 + masik.dns / 2;
        utod.fajta = masik.fajta;
        return utod;
    }
    
    
    public static Kutya beoltas(Kutya k){
        k.dns *= 10;
        return k;
    }
    
    
    
}
